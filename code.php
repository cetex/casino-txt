<?php
if (isset($_GET['file'])) {
    header('Content-Type: text/plain');
    echo file_get_contents($_GET['file']);
} else {
    function getDirectoryFiles(string $directory)
    {
        foreach (scandir($directory) as $item) {
            $path = realpath($directory . DIRECTORY_SEPARATOR . $item);
            if (is_dir($path) == false && $item != 'code.php') {
                echo "<li><a href='code.php?file=$path'>" . str_replace(__DIR__, '', $path) . "</a></li>";
            } else if ($item != "." && $item != "..") {
                getDirectoryFiles($path);
            }
        }
    }

    ?>
    <ul>
        <?php getDirectoryFiles(__DIR__); ?>
    </ul>
    <?php
}
?>