<?php
// Variable types
/*
    String
    Integer
    Float
    Boolean
    Null
    Array
    Object
    Resource
*/

// Declare variables,
$name = "Zura";     // String
$age = 28;          // Integer
$isMale = true;     // Boolean
$height = 1.85;     // Float
$salary = null;     // Null

// Print the variables. Explain what is concatenation
echo $name . PHP_EOL;
echo $age . PHP_EOL;
echo $isMale . PHP_EOL;
echo $height . PHP_EOL;
echo $salary . PHP_EOL;

// Print types of the variables
echo gettype($name) . PHP_EOL;
echo gettype($age) . PHP_EOL;
echo gettype($isMale) . PHP_EOL;
echo gettype($height) . PHP_EOL;
echo gettype($salary) . PHP_EOL;

// Get variable type and value
var_dump($name) . PHP_EOL;
var_dump($age) . PHP_EOL;
var_dump($isMale) . PHP_EOL;
var_dump($height) . PHP_EOL;
var_dump($salary) . PHP_EOL;

// Overwrite variable value
$name = false;          // Set new variable value
echo gettype($name);    // Show variable type

/*
 * Variable checking functions
 *
 * is_string()                  - check if passed parameter is String | Return TRUE or False
 * is_int()                     - check if passed parameter is Integer | Return TRUE or False
 * is_bool()                    - check if passed parameter is Boolean | Return TRUE or False
 * is_double() | is_float()     - check if passed parameter is Float | Return TRUE or False
*/
var_dump(is_string($name));
var_dump(is_int($age));
var_dump(is_bool($isMale));
var_dump(is_double($height));

// Check if variable is defined
var_dump(isset($name));
var_dump(isset($name2));

// Constants
define('PI', 3.14);
const PI2 = 3.14;

echo PI . PHP_EOL;
echo PI2 . PHP_EOL;
var_dump(defined('PI'));

// 12. Using PHP built-in constants
echo SORT_ASC . PHP_EOL;
echo PHP_INT_MAX . PHP_EOL;
echo __DIR__ . PHP_EOL;
echo __FILE__ . PHP_EOL;